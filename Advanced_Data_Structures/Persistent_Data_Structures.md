#Persistent data structures.

+ Time travel geometry.
+ Dynamic optimality
+ Memory hierachy
+ Hashing
+ Integers
+ Dynamic Graphs
+ Strings
+ Succinct.


## Time travel geometry.

### Pointer machine.

+ Temporal data structure:
	+ Persistence
		+ No destroy.
	+ Retroactivity
		+ Make changues, return back.

#### Persistence:
Remember everything (different version of the data structure).

Every update makes & returns a new version.

Keep all versions of DS.

All DS are relatives to the specify version.

+ Leves:
	+ Partial persistences:
		+ Update only the latest version. 
		+ Linear order.
			
				---|------|----|--O
	+ Full persistence:
		+ Update any version.
		+ Versoin tree
			
					   x--|--
			     --|--/
	+ Confluence persistence:
		+ Combine two versions to create new versions.
		+ Version form DAG.
		
		
					   x--|---\
			     --|--/			---|----
					  \		  /
					   x--|--
	+ Functional:
		+ No modify.
		+ Only make new nodes.
		+ Nodes vs Versions.


##### Partial Persistence

Any pointer-machine DS.

Constant number pointer inside of the node.

(With <= p = 0(1) pointers to any node.)

